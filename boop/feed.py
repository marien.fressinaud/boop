import uuid


class Feed:
    def __init__(self, slug, articles, meta={}, configuration={}):
        self.meta = {}
        self.meta.update(configuration)
        self.meta["FEED_SLUG"] = slug
        self.meta["FEED_TITLE"] = configuration["SITE_TITLE"]
        self.meta["FEED_SITE_LINK"] = configuration["SITE_URL"]
        self.meta["FEED_HUB_LINK"] = configuration["SITE_WEBSUB_HUB"]
        for key, value in meta.items():
            feed_key = f"FEED_{key.upper()}"
            self.meta[feed_key] = value
        self.meta["FEED_ARTICLES"] = articles

    def slug(self):
        return self.meta["FEED_SLUG"]

    def title(self):
        return self.meta.get("FEED_TITLE", "A feed")

    def url(self):
        site_url = self.meta.get("SITE_URL", "")
        return f"{site_url}{self.slug()}.atom.xml"

    def site_link(self):
        return self.meta["FEED_SITE_LINK"]

    def hub_link(self):
        return self.meta["FEED_HUB_LINK"]

    def uuid(self):
        return str(uuid.uuid5(uuid.NAMESPACE_URL, self.url()))

    def articles(self):
        return self.meta["FEED_ARTICLES"]
