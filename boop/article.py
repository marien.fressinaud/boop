import markdown
import datetime
import uuid

import pytz


class Article:
    def __init__(self, slug, article_content, configuration={}, serie=None):
        # We convert the file's content into HTML
        md = markdown.Markdown(
            extensions=[
                "codehilite",
                "extra",
                "meta",
            ],
            extension_configs={"codehilite": {"css_class": "highlight"}},
        )
        content = md.convert(article_content)

        # We get the local variables from the Markdown file (metadata)
        # which will be accessible in the article template
        self.meta = {}
        self.meta.update(configuration)
        self.meta["ARTICLE_SLUG"] = slug
        self.meta["ARTICLE_TEMPLATE"] = "article"
        for key, values in md.Meta.items():
            article_key = f"ARTICLE_{key.upper()}"
            # The Markdown's meta extension extract all the values in arrays, even
            # if there is only one value.
            if len(values) > 1:
                self.meta[article_key] = values
            else:
                self.meta[article_key] = values[0]
        self.meta["ARTICLE_CONTENT"] = content
        self.meta["ARTICLE_SERIE"] = serie
        self.meta["ARTICLE_LAST_MODIFICATION"] = datetime.datetime.now()

    def render(self):
        return self.template.render(self.meta)

    def slug(self):
        return self.meta["ARTICLE_SLUG"]

    def title(self):
        return self.meta.get("ARTICLE_TITLE", "An article")

    def author(self):
        if "ARTICLE_AUTHOR" in self.meta:
            return self.meta["ARTICLE_AUTHOR"]
        elif "SITE_AUTHOR" in self.meta:
            return self.meta["SITE_AUTHOR"]
        else:
            return None

    def url(self):
        site_url = self.meta.get("SITE_URL", "")
        return f"{site_url}{self.slug()}.html"

    def uuid(self):
        return str(uuid.uuid5(uuid.NAMESPACE_URL, self.url()))

    def content(self):
        return self.meta["ARTICLE_CONTENT"]

    def timezone(self):
        site_timezone = self.meta.get("SITE_TIMEZONE", "UTC")
        return pytz.timezone(site_timezone)

    def on_blog(self):
        return self.meta.get("ARTICLE_BLOG", "true") == "true"

    def is_private(self):
        return self.meta.get("ARTICLE_PRIVATE", "false") == "true"

    def date(self):
        if "ARTICLE_DATE" not in self.meta:
            return datetime.datetime.now(tz=self.timezone())

        article_date = datetime.datetime.strptime(
            self.meta["ARTICLE_DATE"], "%Y-%m-%d %H:%M"
        )
        return article_date.astimezone(tz=self.timezone())

    def update(self):
        if "ARTICLE_UPDATE" not in self.meta:
            return None

        article_update = datetime.datetime.strptime(
            self.meta["ARTICLE_UPDATE"], "%Y-%m-%d %H:%M"
        )
        return article_update.astimezone(tz=self.timezone())

    def serie(self):
        return self.meta["ARTICLE_SERIE"]

    def template(self):
        return self.meta["ARTICLE_TEMPLATE"]

    def last_modification(self):
        return self.meta["ARTICLE_LAST_MODIFICATION"]
