import xml.etree.ElementTree as ET


class Atom:
    def __init__(self):
        self.feed = ET.Element("feed", xmlns="http://www.w3.org/2005/Atom")

    def add_header(self, attributes):
        ET.SubElement(self.feed, "title").text = attributes["title"]
        ET.SubElement(
            self.feed,
            "link",
            href=attributes["self_link"],
            rel="self",
            type="application/atom+xml",
        )
        ET.SubElement(
            self.feed,
            "link",
            href=attributes["site_link"],
            rel="alternate",
            type="text/html",
        )
        ET.SubElement(self.feed, "id").text = attributes["id"]
        ET.SubElement(self.feed, "updated").text = attributes["updated"].isoformat(
            timespec="seconds"
        )

        if "hub_link" in attributes:
            ET.SubElement(self.feed, "link", href=attributes["hub_link"], rel="hub")

    def add_entry(self, attributes):
        entry = ET.SubElement(self.feed, "entry")
        ET.SubElement(entry, "title").text = attributes["title"]
        ET.SubElement(entry, "id").text = attributes["id"]
        if "author" in attributes:
            author = ET.SubElement(entry, "author")
            ET.SubElement(author, "name").text = attributes["author"]
        ET.SubElement(
            entry,
            "link",
            href=attributes["article_link"],
            rel="alternate",
            type="text/html",
        )
        ET.SubElement(entry, "published").text = attributes["published"].isoformat(
            timespec="seconds"
        )
        if "updated" in attributes:
            updated = attributes["updated"]
        else:
            updated = attributes["published"]
        ET.SubElement(entry, "updated").text = updated.isoformat(timespec="seconds")
        ET.SubElement(entry, "content", type="html").text = attributes["content"]

    def write(self, filepath):
        element_tree = ET.ElementTree(self.feed)
        element_tree.write(filepath, encoding="unicode", xml_declaration=True)
