import os
import shutil
import itertools
import datetime

import boop.boopsy as boopsy
from boop.atom import Atom
from boop.article import Article
from boop.page import Page


def init_output_folder(output_path):
    """Create the output directory, empty it if it contains files.
    """
    if os.path.isdir(output_path):
        # it should be a directory...
        shutil.rmtree(output_path)
    elif os.path.exists(output_path):
        # ... but it also can be a simple file if user created it manually!
        os.remove(output_path)
    os.mkdir(output_path)


def build_article_from_filepath(filepath, configuration, serie=None):
    """Read a file and initialize an Article from it.
    """
    last_modification = os.path.getmtime(filepath)
    filepath_no_ext, _ = os.path.splitext(filepath)
    slug = os.path.basename(filepath_no_ext)
    with open(filepath, "r") as article_file:
        article = Article(
            slug, article_file.read(), configuration=configuration, serie=serie
        )
        article.meta["ARTICLE_LAST_MODIFICATION"] = last_modification
        return article


def build_page_from_filepath(filepath, configuration, basename=None, slug=None):
    """Read a file and initialize a Page from it.
    """
    filepath_no_ext, _ = os.path.splitext(filepath)
    if slug:
        final_slug = slug
    elif basename and filepath_no_ext.startswith(basename):
        index_basename = len(basename) + 1
        final_slug = filepath_no_ext[index_basename:]
    else:
        final_slug = os.path.basename(filepath_no_ext)

    with open(filepath, "r") as page_file:
        return Page(final_slug, page_file.read(), configuration=configuration)


def build_blog_page(articles, series, configuration):
    articles.sort(key=lambda article: article.date(), reverse=True)
    series.sort(key=lambda serie: serie.title(), reverse=False)

    slug = configuration["SITE_BLOG_SLUG"]

    blog_page = Page(slug, "", configuration=configuration)
    blog_page.meta["PAGE_TEMPLATE"] = "blog"
    blog_page.meta["ARTICLES"] = articles
    blog_page.meta["ARTICLES_BY_YEAR"] = itertools.groupby(
        articles, key=lambda article: article.date().year
    )
    blog_page.meta["SERIES"] = series
    return blog_page


def build_sitemap_page(articles, series, configuration):
    articles.sort(key=lambda article: article.date(), reverse=True)
    series.sort(key=lambda serie: serie.title(), reverse=False)

    sitemap_page = Page("sitemap", "", configuration=configuration)
    sitemap_page.meta["PAGE_TEMPLATE"] = "sitemap.xml"
    sitemap_page.meta["ARTICLES"] = articles
    sitemap_page.meta["SERIES"] = series
    return sitemap_page


templates_cache = {}


def write_content(content, output_path):
    """Write content (Article or Page) under output_path.
    """
    template_name = content.template()
    if "." in template_name:
        template_ext = os.path.splitext(template_name)[1]
        template_filepath = os.path.join(os.curdir, "templates", template_name)
        output_filepath = os.path.join(output_path, f"{content.slug()}{template_ext}")
    else:
        template_filepath = os.path.join(
            os.curdir, "templates", f"{template_name}.html"
        )
        output_filepath = os.path.join(output_path, f"{content.slug()}.html")

    # Initialize the template defined within content
    if template_filepath in templates_cache:
        template = templates_cache[template_filepath]
        try:
            rendered_content = template.render(content.meta)
        except Exception as e:
            print(
                f"An error occured when rendering {content.slug()} in {output_filepath}"
            )
            raise e
    elif os.path.exists(template_filepath):
        template = boopsy.Template(template_filepath)
        templates_cache[template_filepath] = template
        try:
            rendered_content = template.render(content.meta)
        except Exception as e:
            print(
                f"An error occured when rendering {content.slug()} in {output_filepath}"
            )
            raise e
    else:
        rendered_content = content.content()

    mkdirs_for_file(output_filepath)
    with open(output_filepath, "w") as output_file:
        output_file.write(rendered_content)


def write_feed(feed, output_path):
    """Write a feed as an Atom feed under output_path.
    """
    feed_filepath = os.path.join(output_path, f"{feed.slug()}.atom.xml")
    articles = feed.articles()

    # We want to sort the articles by publication date
    articles.sort(key=lambda article: article.date(), reverse=True)

    if articles:
        updated = articles[0].update() or articles[0].date()
    else:
        updated = datetime.datetime.now()

    feed_header = {
        "title": feed.title(),
        "self_link": feed.url(),
        "site_link": feed.site_link(),
        "id": f"urn:uuid:{feed.uuid()}",
        "updated": updated,
    }

    if feed.hub_link():
        feed_header["hub_link"] = feed.hub_link()

    atom = Atom()
    atom.add_header(feed_header)

    for article in articles:
        entry = {
            "title": article.title(),
            "id": f"urn:uuid:{article.uuid()}",
            "article_link": article.url(),
            "published": article.date(),
            "content": article.content(),
        }

        article_author = article.author()
        if article_author:
            entry["author"] = article_author
        article_update = article.update()
        if article_update:
            entry["updated"] = article_update

        atom.add_entry(entry)

    mkdirs_for_file(feed_filepath)
    atom.write(feed_filepath)


def list_dirs(path):
    """List all the directories within a path.
    """
    for filename in os.listdir(path):
        full_path = os.path.join(path, filename)
        if os.path.isdir(full_path):
            yield filename


def match_extensions(filename, extensions):
    """Return True if filename extension is part of extensions array.
    """
    if len(extensions) > 0:
        _, ext = os.path.splitext(filename)
        if ext[1:] not in extensions:
            return False
    return True


def list_files(path, only=[]):
    """List all the files within a path.
    """
    for filename in os.listdir(path):
        full_path = os.path.join(path, filename)

        # We don't want to yield directories
        if not os.path.isfile(full_path):
            continue

        # If file extension is not part of "only" array, we don't yield it. If
        # only is empty, it means we accept all extensions.
        if not match_extensions(full_path, only):
            continue

        yield filename


def dir_tree(path, only=[]):
    """List all the tree directory under the given path.
    """
    for filename in os.listdir(path):
        full_path = os.path.join(path, filename)
        if os.path.isdir(full_path):
            for sub_filename in dir_tree(full_path, only=only):
                yield os.path.join(filename, sub_filename)
        elif match_extensions(filename, only):
            yield filename


def mkdirs_for_file(filepath):
    """Create parent directories for a given file.
    """
    path = os.path.dirname(filepath)
    os.makedirs(path, exist_ok=True)
