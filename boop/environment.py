from enum import Enum


class Environment(Enum):
    DEVELOPMENT = 1
    PRODUCTION = 2


def load_from_argv(argv):
    if "--development" in argv:
        return Environment.DEVELOPMENT
    else:
        return Environment.PRODUCTION
