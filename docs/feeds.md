# Declare your feeds

You might have noticed that Boop! generated automatically [an Atom feed](https://en.wikipedia.org/wiki/Atom_(Web_standard))
under `_site/feeds/all.atom.xml`. This comes for free because you should
always generate a feed of your blogs.

You can (and should) declare your feed in your templates by adding the
following line in the `<head>` element:

```html
<link href="{{ SITE_URL }}feeds/all.atom.xml" type="application/atom+xml" rel="alternate" title="{{ SITE_TITLE }}" />
```

To declare the feed of a serie in [a `serie` template](/docs/series.md), you
can use this:

```html
<link href="{{ FEED.url() }}" type="application/atom+xml" rel="alternate" title="{{ FEED.title() }}" />
```
