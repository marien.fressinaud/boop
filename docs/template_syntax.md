## Learn the template syntax

As explained in the rest of the documentation, you can create templates under
the `templates/` folder. These templates are basic HTML files, with a pinch of
special syntax.

If you're curious, you can find the template system in [the `boop/boopsy.py`
file](/boop/boopsy.py), it's less than 200 lines.

## Output dynamic values

To output the value of a variable, we already saw how to do it:

```html
<p>
    {{ ARTICLE_CONTENT }}
</p>
```

You can use any valid Python expression between `{{ }}`. You have access to the
variables defined in the meta header, the Python builtin functions and the
`datetime` module.

## Format dates

You might want to display the article publication date in a human-readable way
(article template):

```html
<div class="article-pubdate">
    {{ datetime.strptime(ARTICLE_DATE, "%Y-%m-%d %H:%M").strftime("%a %d %B %Y")
</div>
```

Is it awful? Yes it is! Hopefully, you will not have to call this very often.

## Conditions

Display content conditionnaly with a `if` statement:

```html
<head>
    {% if PAGE_TITLE %}
        <title>{{ PAGE_TITLE }} - {{ SITE_TITLE }}</title>
    {% else %}
        <title>{{ SITE_TITLE }}</title>
    {% endif %}
</head>
```

`elif` statement is also supported the same way as `if`.

## Loops

The `for` statement gives you the possibility to generate lists:

```html
<ul>
    {% for author in ARTICLE_AUTHORS %}
        <li>{{ author }}</li>
    {% enfor %}
</ul>
```

## Declare variables

Sometimes, it's handy to declare an additional variable in the template: you
can do it with a `set` statement. For instance, to list only articles with a
`featured` tag (blog template):

```html
{% set articles = [a for a in ARTICLES if "featured" in a.meta.get('ARTICLE_TAGS', []) %}

<ul>
    {% for article in articles %}
        <li>
            <a href="{{ article.url() }}">{{ article.title() }}</a>
            ({{ article.date().strftime("%d %b %Y") }})
        </li>
    {% endfor %}
</ul>
```

Note that `article.date()` returns a `datetime`, so we don't need to call
`strptime` on it. It's a bit easier than in the `article` template.
