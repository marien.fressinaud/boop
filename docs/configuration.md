# Configure Boop!

Boop! can be configured by creating a `configuration.yml` file. As the
extension suggests it, this file must contain YAML content.

## Default settings

Five information are used by the Atom feed, so you may want to declare them:

```yaml
url: "http://my-website.org/"
title: "My website"
author: "Dale Cooper"
timezone: "Europe/Paris"
uuid: "d836fc75-3412-5caf-91e2-2fad31343882"
```

You can also declare an optional `subtitle` variable to describe your website.

Concerning the `uuid` variable, you might have noticed that Boop! warned you
of its absence when you were calling the `boop.py` command before. You can
copy/paste the value it suggested you, then it will stop to display the
warning.

You can set a `blog_slug` to change where the main blog page will be generated
(e.g. `blog_slug: "index"` to have it as the home page).

`url` can be set to `filesystem` during development.

You can declare as many variables as you want, they all will be accessible in
the templates, uppercased and prepended by `SITE_`. Your best ally will be
`SITE_URL` because it allows you to link other files by their absolute path.

## Development settings

If you want to use a different configuration in development, you can specify
different options under a `development` section:

```yaml
url: "http://my-website.org/"
title: "My website"
author: "Dale Cooper"
timezone: "Europe/Paris"

development:
  url: "filesystem"
  title: "[dev] My website"
```

By default, `boop.py` command generates the website with production
configuration. To use the development one you should pass the `--development`
argument:

```console
$ boop.py --development
```
