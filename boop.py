#!/bin/env python3

import os
import sys
import locale
import pickle
import shutil
import time

import boop.environment as environment
import boop.configuration as config
import boop.utils as utils
from boop.feed import Feed


locale.setlocale(locale.LC_ALL, "")


def main(environment):
    start = time.perf_counter()

    # Make sure ./_site folder is empty
    output_path = os.path.join(os.curdir, "_site")
    utils.init_output_folder(output_path)

    # Init cache dir
    cache_path = os.path.join(os.curdir, "_cache")
    if not os.path.exists(cache_path):
        os.mkdir(cache_path)

    # Load the configuration (it can be overidden by a configuration.yml file)
    configuration_path = os.path.join(os.curdir, "configuration.yml")
    configuration = config.load(configuration_path, environment, output_path)

    # STEP 1: we load all the Articles, Pages and Feeds
    pages = []
    series = []
    articles = {}
    feeds = []

    # Check that index file exists
    index_filepath = os.path.join(os.curdir, "index.html")
    if os.path.exists(index_filepath):
        index_page = utils.build_page_from_filepath(
            index_filepath, configuration, basename=os.curdir
        )
        pages.append(index_page)

    # Check that pages directory exists and load Pages from it
    pages_path = os.path.join(os.curdir, "pages")
    if os.path.isdir(pages_path):
        for filename in utils.dir_tree(pages_path, only=["html"]):
            page_filepath = os.path.join(pages_path, filename)
            page = utils.build_page_from_filepath(
                page_filepath, configuration, basename=pages_path
            )
            pages.append(page)

    # Load articles from the cache
    articles_cache_path = os.path.join(cache_path, "articles.pickle")
    if os.path.exists(articles_cache_path):
        with open(articles_cache_path, "rb") as cache_file:
            articles = pickle.load(cache_file)

    # Check that articles directory exists and load Articles from it
    articles_path = os.path.join(os.curdir, "articles")
    if os.path.isdir(articles_path):
        dirs_with_articles = list(utils.list_dirs(articles_path)) + ["."]
        for dirname in dirs_with_articles:
            dirpath = os.path.realpath(os.path.join(articles_path, dirname))

            serie_filepath = os.path.join(dirpath, "serie.html")
            serie = None

            # If the directory contains a `serie.html` file, it means it
            # contains a serie so we load a page from it.
            if dirname != "." and os.path.exists(serie_filepath):
                slug = f"serie/{dirname}"
                serie = utils.build_page_from_filepath(
                    serie_filepath, configuration, slug=slug
                )
                series.append(serie)

            # Let's collect all the articles from the directory
            serie_articles = []
            for filename in utils.list_files(dirpath, only=["md"]):
                article_filepath = os.path.join(dirpath, filename)

                reload_article = True
                if article_filepath in articles:
                    # the article is in cache
                    article = articles[article_filepath]
                    last_modification = os.path.getmtime(article_filepath)
                    # if the two last_modification are different, the file has
                    # changed and we need to reload the article from the file.
                    reload_article = article.last_modification() != last_modification

                if reload_article:
                    article = utils.build_article_from_filepath(
                        article_filepath, configuration, serie
                    )
                    articles[article_filepath] = article

                if not article.is_private():
                    serie_articles.append(article)

            if serie:
                # If the directory contains a serie, we load a feed for the serie
                # and we let know the serie about its articles and feed.

                serie.set_articles(serie_articles)

                if serie_articles or configuration.get("SITE_FORCE_FEED", False):
                    feed_slug = f"feeds/{dirname}"
                    feed_title = f"{serie.title()} - {configuration['SITE_TITLE']}"
                    serie_feed = Feed(
                        feed_slug,
                        serie_articles,
                        {"title": feed_title, "site_link": serie.url()},
                        configuration=configuration,
                    )
                    feeds.append(serie_feed)
                    serie.set_feed(serie_feed)

    # Load a "blog" page which has access to the list of articles
    blog_template_filepath = os.path.join("templates", "blog.html")
    blog_articles = [
        article
        for article in articles.values()
        if not article.is_private() and article.on_blog()
    ]
    if os.path.exists(blog_template_filepath):
        blog_page = utils.build_blog_page(blog_articles, series, configuration)
        pages.append(blog_page)

    # Load a "sitemap.xml" page
    sitemap_template_filepath = os.path.join("templates", "sitemap.xml")
    if os.path.exists(sitemap_template_filepath):
        sitemap_page = utils.build_sitemap_page(blog_articles, series, configuration)
        pages.append(sitemap_page)

    # And load the main feed
    if len(blog_articles) > 0 or configuration.get("SITE_FORCE_FEED", False):
        feed = Feed("feeds/all", blog_articles, configuration=configuration)
        feeds.append(feed)

    # STEP 2: check slugs are not duplicated
    slugs = set()
    for content in pages + series + feeds + list(articles.values()):
        slug = content.slug()
        if slug in slugs:
            raise ValueError(f"Slug is duplicated: {slug}")
        else:
            slugs.add(slug)

    # STEP 3: we write all the pages, articles and feeds
    for page in pages + series:
        utils.write_content(page, output_path)
        print(f"Written page: {page.url()}")

    for feed in feeds:
        utils.write_feed(feed, output_path)
        print(f"Written feed: {feed.url()}")

    articles_sorted = list(articles.values())
    articles_sorted.sort(key=lambda article: article.date())
    for article in articles_sorted:
        utils.write_content(article, output_path)
        print(f"Written article: {article.url()}")

    # STEP 4: and we finish by copying the static folder.
    static_dirpath = os.path.join(os.curdir, "static")
    if os.path.isdir(static_dirpath):
        shutil.copytree(static_dirpath, output_path, dirs_exist_ok=True)
        print(f"Static files copied")

    # STEP 5: save cache!
    with open(articles_cache_path, "wb") as cache_file:
        pickle.dump(articles, cache_file)

    ellapsed = time.perf_counter()
    print(f"Generated in {ellapsed - start:.2f}s")


if __name__ == "__main__":
    environment = environment.load_from_argv(sys.argv)
    main(environment)
    print("Boop!")
