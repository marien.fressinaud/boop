# Boop! — a simple static site generator, for fun.

**Boop! is a static site generator that I built for my personal needs,** mostly
to learn how to create such a tool. You should probably not use it, or at your
own risks.

**It is designed to be used by Web developers who are comfortable with a
terminal.** Indeed, nothing comes for free in Boop!: you'll have to create your
own templates and your own style, you'll need to setup additional tooling by
yourself, there's nothing like “hot reloading”, etc.

**Boop! is now done. It means no new feature will be added, but it's still
maintained.** I've been using it for 3 years and didn't change anything for more
than a year: I'm happy with the current version. It doesn't mean that I'm being
satisfied with the implementation choices! (some are really weird :)) However,
I may encounter some bugs or security issues: I intend to fix them.

## Installation

In this guide, it is assumed that you are using Linux.

First, clone the Boop! repository and install its dependencies. Boop! requires
Python 3.6 to work.

```console
$ git clone https://framagit.org/marien.fressinaud/boop.git
$ cd boop
boop$ pip3 install -r requirements.txt
```

Then, add the path of the repository to your `PATH` environment variable. For
instance, you can add it to your `.bashrc` file:

```console
boop$ # Be sure to be in the boop/ folder
boop$ echo export PATH="$(pwd):\$PATH" >> ~/.bashrc
boop$ source ~/.bashrc
```

## Basic usage

First, create a project and execute the Boop! command:

```console
$ mkdir my-website && cd my-website
my-website$ echo '<h1>Welcome!</h1>' > index.html
my-website$ boop.py
Warning: you should set a uuid in your configuration file. Here’s one for you: d836fc75-3412-5caf-91e2-2fad31343882
Written page: file:///path/to/my-website/_site/index.html
Generated in 0.00s
Boop!
```

Your "production-ready" website has been generated under a `_site/` folder.
You can open it in your browser:

```console
my-website$ xdg-open _site/index.html
```

**Remember: each time you make a change, you'll have to call the `boop.py`
command!** Also, never directly change the `_site/*` files, they'll be
overwritten.

In this quick tutorial, you basically learn how to copy an `index.html` file
under a `_site/` folder with Boop! Obviously, it can do a whole bunch of other
things. The documentation will guide you to learn more.

## Full documentation

1. [Write articles](/docs/articles.md)
1. [Create an index of your articles](/docs/blog.md)
1. [Write pages](/docs/pages.md)
1. [Manage static files](/docs/static.md)
1. [Configure Boop!](/docs/configuration.md)
1. [Create series](/docs/series.md)
1. [Declare your feeds](/docs/feeds.md)
1. [Learn the template syntax](/docs/template_syntax.md)

## Real-life examples

- [marienfressinaud.fr](https://framagit.org/marienfressinaud/next.marienfressinaud.fr)
- [flus.fr/carnet](https://github.com/flusio/carnet)
- [status.flus.io](https://github.com/flusio/status.flus.io)

## Tests (for Boop! developers)

Boop! comes with few tests (i.e. doctests). They can be executed to check that
everything works smoothly:

```console
$ python3 -m doctest -vf */*.py
```
